package com.kytms.system.service;

import com.kytms.core.model.CommModel;
import com.kytms.core.model.JgGridListModel;
import com.kytms.core.service.BaseService;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 *
 * Menu
 *
 * @author 臧英明
 * @create 2017-11-18
 */
public interface LoginInfoService<LoginInfo> extends BaseService<LoginInfo>{
    JgGridListModel getLoginInfoList(CommModel comm);

}
