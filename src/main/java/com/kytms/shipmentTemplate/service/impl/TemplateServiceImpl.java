package com.kytms.shipmentTemplate.service.impl;

import com.kytms.core.entity.*;
import com.kytms.core.model.CommModel;
import com.kytms.core.model.JgGridListModel;
import com.kytms.core.service.impl.BaseServiceImpl;
import com.kytms.core.utils.SessionUtil;
import com.kytms.core.utils.SpringUtils;
import com.kytms.shipmentTemplate.dao.TemplateDao;
import com.kytms.shipmentTemplate.dao.impl.TemplateLedgerDTDaoImpl;
import com.kytms.shipmentTemplate.dao.impl.TemplateLedgerDaoImpl;
import com.kytms.shipmentTemplate.service.TemplateService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 运单模板SERVICE实现类
 * @author 陈小龙
 * @create 2018-04-09
 */
@Service(value = "TemplateService")
public class TemplateServiceImpl extends BaseServiceImpl<ShipmentTemplate> implements TemplateService<ShipmentTemplate> {
    private final Logger log = Logger.getLogger(TemplateServiceImpl.class);//输出Log日志
    private TemplateDao<ShipmentTemplate> templateDao;
    @Resource(name = "TemplateDao")
    public void setTemplateDao(TemplateDao<ShipmentTemplate> templateDao) {
        super.setBaseDao(templateDao);
        this.templateDao = templateDao;
    }




    public JgGridListModel getList(CommModel commModel) {
        String where = " and organization.id ='" + SessionUtil.getOrgId() +"'";
        String orderBY = " ORDER BY  create_time desc";
        return super.getListByPage(commModel,where,orderBY);
    }

    public ShipmentTemplate saveTemplate(ShipmentTemplate shipmentTemplate) {
        shipmentTemplate.setOrganization(SessionUtil.getOrg());//绑定组织机构

        List<TemplateLedger> ledgers = shipmentTemplate.getTemplateLedgers();//获取台账信息
        shipmentTemplate.setTemplateLedgers(null);

        ShipmentTemplate shipmentTemplate1 = templateDao.savaBean(shipmentTemplate);

        templateDao.executeHql(" Delete FROM JC_TEMPLATE_LEDGER Where shipmentTemplate.id = '"+shipmentTemplate1.getId()+"'",null);
        TemplateLedgerDaoImpl templateLedgerDao = SpringUtils.getBean(TemplateLedgerDaoImpl.class); //存储台账信息
        for (TemplateLedger ledger:ledgers) {
            ledger.setOrganization(SessionUtil.getOrg());
            //处理货品明细
            List<TemplateLedgerDetails> ledgerDetails = ledger.getTemplateLedgerDetails();//获取台账明细
            ledger.setTemplateLedgerDetails(null);
            ledger.setShipmentTemplate(shipmentTemplate1);
            TemplateLedger resule2 = templateLedgerDao.savaBean(ledger);
            TemplateLedger resule = templateLedgerDao.selectBean(resule2.getId());
            resule.setTemplateLedgerDetails(null); //阻断关系
            TemplateLedgerDTDaoImpl templateLedgerDTDao = SpringUtils.getBean(TemplateLedgerDTDaoImpl.class); //存储明细
            for (TemplateLedgerDetails ledgerDetail:ledgerDetails) {
                ledgerDetail.setTemplateLedger(resule);
                templateLedgerDTDao.savaBean(ledgerDetail);
            }
        }
        return shipmentTemplate1;
    }
}
